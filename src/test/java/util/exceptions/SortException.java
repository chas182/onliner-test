package util.exceptions;

/**
 * @author vabramov
 */
public class SortException extends RuntimeException {

    public SortException() {
    }

    public SortException(String message) {
        super(message);
    }
}
