package util;

/**
 * This class is used only for onliner.by
 * @author vabramov
 */
public class BuildXpath {
    public static String xpathForSelect(String[] info) {
        return xpath("select", info);
    }

    public static String xpathForInput(String[] info) {
        return xpath("input", info);
    }

    private static String xpath(String tagName, String[] info) {
        StringBuilder xpath = new StringBuilder("//" + tagName + "[contains(@name, 'sp");
        for (int i = 1; i < info.length; ++i) {
            xpath.append('[').append(info[i]).append(']');
        }
        return xpath.append("')]").toString();
    }
}
