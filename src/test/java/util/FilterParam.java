package util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

/**
 * @author vabramov
 */
public class FilterParam {
    private String filterText;
    private String textOnProductPage;
    private int from;
    private int to;
    private String value;
    private boolean select = false;

    public FilterParam(String filterText, String textOnProductPage, String value) {
        this.filterText = filterText;
        this.textOnProductPage = textOnProductPage;
        this.value = value;
        this.select = true;
    }

    public FilterParam(String filterText, String textOnProductPage, int from, int to) {
        this.filterText = filterText;
        this.textOnProductPage = textOnProductPage;
        this.from = from;
        this.to = to;
    }

    public String getTextOnProductPage() {
        return textOnProductPage;
    }

    public void setTextOnProductPage(String textOnProductPage) {
        this.textOnProductPage = textOnProductPage;
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public List<WebElement> getElementInFilterSection(RemoteWebDriver driver) {
        if (select) {
            return driver.findElements(By.xpath("//*[text()='" + filterText + "']/../following-sibling::td/select"));
        } else if (filterText.contains("Цена")) {
            return driver.findElements(By.xpath("//*[text()='" + filterText + "']/following-sibling::td/input"));
        } else {
            return driver.findElements(By.xpath("//*[text()='" + filterText + "']/../following-sibling::td/input"));
        }
    }

    public String getValueOnProductPage(RemoteWebDriver driver) {
        List<WebElement> list;
        if (filterText.contains("Цена")) {
            list = driver.findElements(By.xpath("//div[@class='ppdescr']/following-sibling::div[1]//td[1]"));
        } else {
            list = driver.findElements(By.xpath("//*[contains(text(),'" + textOnProductPage + "')]/../../following-sibling::td"));
        }
        return list.isEmpty() ? "" : list.get(0).getText();
    }
}
