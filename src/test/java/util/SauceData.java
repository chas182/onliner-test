package util;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

/**
 * @author vabramov
 */
public class SauceData {

    public static final String[] categories = {"Телефоны", "Фото и Видео", "Компьютеры", "Музыкальные инструменты и оборудование", "Сетевое оборудование", "Встраиваемая техника", "Телевизоры, видео и игровые приставки", "Аудиотехника"};

    @DataProvider(name = "subCategories")
    public static Object[][] getSubCategories(Method testMethod) {
        return new Object[][]{
                new Object[]{new DriverMetaData("internet explorer", "11", "Windows 8.1"),"Телефоны", new String[]{"Мобильные телефоны","Чехлы для телефонов","Дата-кабели","Bluetooth-гарнитуры","3G-модемы"}},
                new Object[]{new DriverMetaData("safari", "6", "OSX 10.8"),"Телефоны", new String[]{"Мобильные телефоны","Чехлы для телефонов","Дата-кабели","Bluetooth-гарнитуры","3G-модемы"}},
                new Object[]{new DriverMetaData("firefox", "27", "Linux"),"Аудиотехника", new String[]{"MP3-плееры","Мультимедийные док-станции","Микрофоны","Диктофоны"}
                }
        };
    }

    @DataProvider(name = "categories")
    public static Object[][] getCategories(Method method) {
        return new Object[][]{
                new Object[]{
                        new DriverMetaData("internet explorer", "11", "Windows 8.1"), categories},
                new Object[]{
                        new DriverMetaData("safari", "6", "OSX 10.8"), categories},
                new Object[]{
                        new DriverMetaData("firefox", "27", "Linux"), categories}
        };
    }
}
