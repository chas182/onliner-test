package util;

import java.util.List;

/**
 * @author vabramov
 */
public class SearchUtil {

    public static boolean containsWord(List<String> searchLinksText, String word) {
        for (String text : searchLinksText) {
            if (!text.contains(word)) {
                return false;
            }
        }
        return true;
    }
}
