package util;

import org.testng.annotations.DataProvider;
import util.ProductPageElement;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author vabramov
 */
public class Data {

    @DataProvider(name = "subCategories")
    public static Object[][] getSubCategories() {
        return new Object[][] {
                {"Телефоны", new String[] {
                        "Мобильные телефоны",
                        "Чехлы для телефонов",
                        "Дата-кабели",
                        "Bluetooth-гарнитуры",
                        "3G-модемы"}
                },
                {"Аудиотехника", new String[] {
                        "MP3-плееры",
                        "Мультимедийные док-станции",
                        "Микрофоны",
                        "Диктофоны"}
                }
        };
    }

    @DataProvider(name = "categories")
    public static Object[][] getCategories() {
        return new Object[][] {
                {new String[]{"Телефоны",
                            "Фото и Видео",
                            "Компьютеры",
                            "Музыкальные инструменты и оборудование",
                            "Сетевое оборудование",
                            "Встраиваемая техника",
                            "Телевизоры, видео и игровые приставки",
                            "Аудиотехника"}},
                };
    }

    @DataProvider(name = "searchPhrases")
    public static Object[][] getSearchPhrases() {
        return new Object[][] {{"Nokia"},{"Samsung"},{"Bosch"}};
    }

    @DataProvider(name = "productParams")
    public static Object[][] getProductParams() {
        return new Object[][] {
                {"Nokia", new ProductPageElement[]{
                    ProductPageElement.DESCRIPTION,
                    ProductPageElement.HEADER,
                    ProductPageElement.IMAGE,
                    ProductPageElement.DEV_LINKS,
                }},
                {"Samsung", new ProductPageElement[]{
                        ProductPageElement.HEADER,
                        ProductPageElement.IMAGE,
                        ProductPageElement.DEV_LINKS,
                        ProductPageElement.RATE,
                }}};
    }

    @DataProvider(name = "filterParams")
    public static Object[][] getFilterParams() {
        return new Object[][] {
                {"Samsung", new FilterParam[]{
                        new FilterParam("Цена (минимальная)", "Цена", 100, 200),
                        new FilterParam("Количество пикселей камеры","Количество точек матрицы", 1, 10),
                        new FilterParam("Количество ядер процессора","Количество ядер", 1, 5),
                        new FilterParam("Тип", "Тип", "смартфон")
                    }
                }
        };
    }
}
