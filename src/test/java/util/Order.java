package util;

import org.openqa.selenium.WebElement;

/**
 * @author vabramov
 */
public enum Order {
    DIRECT_SORT("a-z"),
    REVERSE_SORT("z-a");

    private final String cssClassName;

    Order(String cssClassName) {
        this.cssClassName = cssClassName;
    }

    public String getCssClassName() {
        return cssClassName;
    }

    public static Order getOrder(WebElement webElement) {
        String classAttr = webElement.getAttribute("class");
        if (classAttr.contains(DIRECT_SORT.cssClassName)) {
            return DIRECT_SORT;
        } else if (classAttr.contains(REVERSE_SORT.cssClassName)) {
            return REVERSE_SORT;
        }
        return null;
    }
}
