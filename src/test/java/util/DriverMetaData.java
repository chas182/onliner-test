package util;

/**
 * This class is used in {@code dataProvider} for simplify initialization of {@code WebDriver}.
 *
 * @author vabramov
 * @see org.openqa.selenium.WebDriver
 */
public class DriverMetaData {
    /**
     * Browser name.
     *
     * @see org.openqa.selenium.remote.DesiredCapabilities#setBrowserName(String)
     * @see org.openqa.selenium.remote.DesiredCapabilities#setCapability(String, String)
     */
    private String browser;
    /**
     * Browsers version.
     *
     * @see org.openqa.selenium.remote.DesiredCapabilities#setVersion(String)
     * @see org.openqa.selenium.remote.DesiredCapabilities#setCapability(String, String)
     */
    private String version;
    /**
     * Operation system that will run web browser.
     *
     * @see org.openqa.selenium.remote.DesiredCapabilities#setPlatform(org.openqa.selenium.Platform)
     * @see org.openqa.selenium.remote.DesiredCapabilities#setCapability(String, org.openqa.selenium.Platform)
     */
    private String os;

    public DriverMetaData(String browser, String version, String os) {
        this.browser = browser;
        this.version = version;
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public String getVersion() {
        return version;
    }

    public String getOs() {
        return os;
    }
}
