package util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.List;

/**
 * @author vabramov
 */
public enum ProductPageElement {
    HEADER(By.xpath("//td[@class='product_h1']//span")),
    IMAGE(By.xpath("//td[@class='ppimage']")),
    RATE(By.xpath("//div[@class='pprate']")),
    DESCRIPTION(By.xpath("//div[@class='ppdescr']")),
    DEV_LINKS(By.xpath("//div[@class='dev_links']//li/a | //div[@class='dev_links']//li/c"))
    ;

    private final By locator;

    ProductPageElement(By locator) {
        this.locator = locator;
    }

    public static List<WebElement> getElements(RemoteWebDriver driver, ProductPageElement element) {
        return driver.findElements(element.locator);
    }
}