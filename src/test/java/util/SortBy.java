package util;

/**
 * @author vabramov
 */
public enum SortBy {
    DATE("Дате"),
    POPULARITY("Популярности"),
    PRICE("Цене"),
    RATING("Рейтингу"),
    TITLE("Названию");

    private final String linkText;

    SortBy(String linkText) {
        this.linkText = linkText;
    }

    public String getLinkText() {
        return linkText;
    }
}
