package test;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.*;
import util.Data;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class CatalogTest {
    private Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());
    private MainPage mainPage;
    private CatalogPage catalogPage;
    private long start;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        logger.info("Start Test case");
        start = System.currentTimeMillis();

        mainPage = new MainPage(new FirefoxDriver());

        logger.info("test initialized in " + (System.currentTimeMillis() - start) + " ms");
    }

    @BeforeMethod(alwaysRun = true)
    public void before(Method method) {
        logger.info("'{}' is started.", method.getName());
        if (catalogPage == null) {
            catalogPage = mainPage.open().navigation().navigateToCatalog();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void after(Method method, ITestResult result) {
        logger.info("'{}' is ended", method.getName());
        logger.info("result is: '{}'", (result.isSuccess()) ? "successful" : "failed");
    }

    @Test(dataProvider = "categories", dataProviderClass = Data.class, groups = "catalog")
    public void testCategories(String[] categories) {
        List<String> categoryNames = catalogPage.getCategoriesNames();

        logger.debug("Categories on catalogPage : {}", categoryNames);
        logger.debug("Categories for test : {}", categories);

        Assert.assertTrue(categoryNames.containsAll(Arrays.asList(categories)));
    }

    @Test(dataProvider = "subCategories", dataProviderClass = Data.class, groups = "catalog")
    public void testSubCategories(String category, String[] subCategories) {
        List<String> categoryNames = catalogPage.getSubCategoriesNames(category);

        logger.debug("Sub categories for category '" + category + "' : {}", categoryNames);
        logger.debug("Sub categories for test : {}", subCategories);

        Assert.assertTrue(categoryNames.containsAll(Arrays.asList(subCategories)));
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("Test case is complete in {} ms", (System.currentTimeMillis() - start));
        mainPage.getDriver().quit();
    }
}  