package test;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.CatalogPage;
import pages.ProductPage;
import pages.SearchResultPage;
import util.Data;
import util.Order;
import util.ProductPageElement;
import util.SortBy;

import java.lang.reflect.Method;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.everyItem;

/**
 * @author vabramov
 */
public class SearchTest {

    private Logger logger = LoggerFactory.getLogger(SearchTest.class);
    private CatalogPage catalogPage;

    @Test(groups = "search", dataProviderClass = Data.class, dataProvider = "searchPhrases")
    public void testSearch(String searchWord) {
        logger.info("Test search '{}'", searchWord);
        SearchResultPage resultPage = catalogPage.search(searchWord);

        logger.info("Sort products by price from 'Z' to 'A'");
        resultPage = resultPage.sort(SortBy.PRICE, Order.REVERSE_SORT);

        logger.info("Find search phrase '{}' on page", searchWord);

        assertThat(resultPage.getProductsLinksText(), everyItem(containsString(searchWord)));
    }

    @Test(groups = "search", dataProviderClass = Data.class, dataProvider = "productParams")
    public void testProduct(String searchWord, ProductPageElement[] elements) {
        logger.info("Test search '{}'", searchWord);
        SearchResultPage resultPage = catalogPage.search(searchWord);

        logger.info("Sort products by price from 'Z' to 'A'");
        resultPage = resultPage.sort(SortBy.PRICE, Order.REVERSE_SORT);

        logger.info("Get first product in list");
        ProductPage productPage = resultPage.getFirstProduct();

        Assert.assertTrue(productPage.hasAllNecessaryElements(elements));
    }

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        logger.info("Start Test");
        long start = System.currentTimeMillis();

        catalogPage = new CatalogPage(new FirefoxDriver()).open();

        long end = System.currentTimeMillis();
        logger.info("test initialized in " + (end - start) + " ms");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeCatalog(Method method) {
        logger.info("'{}' is started", method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void after(Method method, ITestResult result) {
        logger.info("'{}' is ended", method.getName());
        logger.info("result is: '{}'", (result.isSuccess()) ? "successful" : "failed");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("Test end");
        catalogPage.getDriver().quit();
    }
}
