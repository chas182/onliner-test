package test.sauce;

import com.saucelabs.testng.SauceOnDemandTestListener;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.CatalogPage;
import util.DriverMetaData;
import util.SauceData;

import java.util.Arrays;
import java.util.List;


@Listeners({SauceOnDemandTestListener.class})
public class CatalogSauceTest extends AbstractSauceTest {
    private Logger logger = LoggerFactory.getLogger(CatalogSauceTest.class);

    @Test(dataProvider = "categories", dataProviderClass = SauceData.class, groups = "catalog")
    public void testCategories(DriverMetaData driverMetaData, String[] categories) throws Exception {
        RemoteWebDriver driver = (RemoteWebDriver) createDriver(driverMetaData);
        CatalogPage catalogPage = new CatalogPage(driver).open();
        List<String> categoryNames = catalogPage.getCategoriesNames();

        logger.info("Categories on catalogPage : {}", categoryNames);
        logger.info("Categories for test : {}", categories);

        Assert.assertTrue(categoryNames.containsAll(Arrays.asList(categories)));
        driver.quit();
    }

    @Test(dataProvider = "subCategories", dataProviderClass = SauceData.class, groups = "catalog")
    public void testSubCategories(DriverMetaData driverMetaData, String category, String[] subCategories) throws Exception {
        RemoteWebDriver driver = (RemoteWebDriver) createDriver(driverMetaData);
        CatalogPage catalogPage = new CatalogPage(driver).open();

        List<String> categoryNames = catalogPage.getSubCategoriesNames(category);

        logger.info("Sub categories for category '" + category + "' : {}", categoryNames);
        logger.info("Sub categories for test : {}", subCategories);

        Assert.assertTrue(categoryNames.containsAll(Arrays.asList(subCategories)));
        driver.quit();
    }

}  