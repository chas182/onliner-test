package test.sauce;

import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.testng.SauceOnDemandAuthenticationProvider;
import com.saucelabs.testng.SauceOnDemandTestListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import util.DriverMetaData;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

import static org.testng.Assert.assertEquals;

/**
 * Class for initializing {@code WebDriver} for cloud testing on platform Sauce Labs.
 *
 * @author vabramov
 * */
public abstract class AbstractSauceTest implements SauceOnDemandSessionIdProvider, SauceOnDemandAuthenticationProvider {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication("abramovvlad182", "dcfa9e3a-3d3c-456e-bc51-ce6f1e0c0e98");

    protected ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();

    protected ThreadLocal<String> sessionId = new ThreadLocal<String>();


    protected WebDriver createDriver(DriverMetaData metaData) throws MalformedURLException {
        logger.info("Creating web driver for cloud testing. Test class: '{}'", getClass().getSimpleName());

        DesiredCapabilities capabilities = new DesiredCapabilities();

        if (logger.isDebugEnabled()) {
            logger.debug("Setting browser name: '{}'", metaData.getBrowser());
        }
        capabilities.setCapability(CapabilityType.BROWSER_NAME, metaData.getBrowser());

        if (metaData.getVersion() != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("Setting browser version: '{}'", metaData.getVersion());
            }
            capabilities.setCapability(CapabilityType.VERSION, metaData.getVersion());
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Setting platform: '{}'", metaData.getOs());
        }
        capabilities.setCapability(CapabilityType.PLATFORM, metaData.getOs());

        capabilities.setCapability("name", getClass().getSimpleName());
        webDriver.set(new RemoteWebDriver(
                new URL("http://" + authentication.getUsername() + ":" + authentication.getAccessKey() + "@ondemand.saucelabs.com:80/wd/hub"),
                capabilities));

        sessionId.set(((RemoteWebDriver) getWebDriver()).getSessionId().toString());

        if (logger.isDebugEnabled()) {
            logger.debug("Web Driver created");
            logger.debug("Session id: '{}'", getSessionId());
        }
        return webDriver.get();
    }


    public WebDriver getWebDriver() {
        System.out.println("WebDriver" + webDriver.get());
        return webDriver.get();
    }

    @Override
    public String getSessionId() {
        return sessionId.get();
    }

    @Override
    public SauceOnDemandAuthentication getAuthentication() {
        return authentication;
    }
}

