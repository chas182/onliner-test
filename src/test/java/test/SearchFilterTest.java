package test;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.CatalogPage;
import pages.ProductPage;
import pages.SearchResultPage;
import util.Data;
import util.FilterParam;

import java.lang.reflect.Method;

/**
 * @author vabramov
 */
public class SearchFilterTest {
    private Logger logger = LoggerFactory.getLogger(SearchFilterTest.class);
    private CatalogPage catalogPage;
    private SearchResultPage searchResultPage;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        logger.info("Start Test");
        long start = System.currentTimeMillis();

        catalogPage = new CatalogPage(new FirefoxDriver());

        long end = System.currentTimeMillis();
        logger.info("test initialized in " + (end - start) + " ms");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeCatalog(Method method) {
        logger.info("'{}' is started", method.getName());
        searchResultPage = catalogPage.openMobilePhones();
    }

    @AfterMethod(alwaysRun = true)
    public void after(Method method, ITestResult result) {
        logger.info("'{}' is ended", method.getName());
        logger.info("result is: '{}'", (result.isSuccess()) ? "successful" : "failed");
    }

    @Test(dataProvider = "filterParams", dataProviderClass = Data.class, groups = "filter")
    public void testCategories(String producer, FilterParam[] params) {
//        SearchResultPage resultPage = searchResultPage.selectProducer(producer).setParams(params);

//        ProductPage productPage = resultPage.getFirstProduct();

//        Assert.assertTrue(productPage.hasAllFilteredParams(params));
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("Test end");
        catalogPage.getDriver().quit();
    }
}
