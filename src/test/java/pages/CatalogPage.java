package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.LinkedList;
import java.util.List;

/**
 * @author vabramov
 */
public class CatalogPage extends ContentPage {

    @FindBy(xpath = "//h1[@class='cm__h1']")
    private List<WebElement> categoriesHeaders;
    private String baseUrl = "http://catalog.onliner.by/";
    private String mobilePhonesUrl = "http://catalog.onliner.by/mobile/";

    public CatalogPage(RemoteWebDriver driver) {
        super(driver);
    }

    private List<WebElement> getSubCategories(String categoryName) {
        return driver.findElements(By.xpath("//div/h1[@class='cm__h1' and text()='" + categoryName + "']/following-sibling::ul[1]/li/div/a[text()!='']"));
    }

    public List<String> getCategoriesNames() {
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieving categories names from '{}'", driver.getCurrentUrl());
        }
        List<String> result = new LinkedList<String>();
        for (WebElement header : categoriesHeaders) {
            String str = header.getText();
            result.add(str.substring(0, str.indexOf(header.findElement(By.tagName("sup")).getText())));
        }
        return result;
    }

    public List<String> getSubCategoriesNames(String categoryName) {
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieving sub categories names from '{}' for category '{}'", driver.getCurrentUrl(), categoryName);
        }
        List<String> result = new LinkedList<String>();
        for (WebElement a : getSubCategories(categoryName)) {
            result.add(a.getText());
        }
        return result;
    }

    public CatalogPage open() {
        getPage(baseUrl);
        return this;
    }

    public SearchResultPage openMobilePhones() {
        getPage(mobilePhonesUrl);
        return new SearchResultPage(driver);
    }
}
