package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * @author vabramov
 */
public class MainPage extends PageBase {

    @FindBy(xpath = "//input[@id='g-search-input']")
    private WebElement searchInput;
    @FindBy(xpath = "//button[@class='top-search-button']")
    private WebElement searchButton;

    public MainPage(RemoteWebDriver driver) {
        super(driver);
    }

    public SearchResultPage search(String whatToType) {
        searchInput.sendKeys(whatToType);
        searchButton.click();
        return new SearchResultPage(driver);
    }

    public MainPage open() {
        getPage(baseUrl);
        return this;
    }
}
