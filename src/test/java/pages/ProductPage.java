package pages;

import org.openqa.selenium.remote.RemoteWebDriver;
import util.FilterParam;
import util.ProductPageElement;

/**
 * @author vabramov
 */
public class ProductPage extends ContentPage {

    public ProductPage(RemoteWebDriver driver) {
        super(driver);
    }

    public boolean hasAllNecessaryElements(ProductPageElement[] elements) {
        for (ProductPageElement element : elements) {
            if (ProductPageElement.getElements(driver, element).isEmpty())
                return false;
        }
        return true;
    }

    public boolean hasAllFilteredParams(FilterParam[] params) {
        for (FilterParam param : params) {
            String value = param.getValueOnProductPage(driver);
            if (param.getFilterText().contains("Цена")) {
                String from = value.substring(0, value.indexOf(' '));
//                String to = value.substring(value.indexOf('-') + 2, value.lastIndexOf(' '));
                if (!checkSelect(param, Integer.parseInt(from)))
                    return false;
            } else {
                if (!value.equals("")) {
                    if (param.isSelect()) {
                        if (!value.contains(param.getValue()))
                            return false;
                    } else {
                        if (!checkSelect(param, Integer.parseInt(value.substring(0,value.indexOf(' ')))) )
                            return false;
                        }
                    }
            }
        }
        return true;
    }

    private boolean checkSelect(FilterParam param, int actualValue) {
        return actualValue >= param.getFrom() && actualValue <= param.getTo();
    }
}
