package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * @author vabramov
 */
public class Navigation extends ContentPage {

    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Каталог и цены']")
    private WebElement catalogLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Технологии")
    private WebElement techLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Авто")
    private WebElement autoLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Автобарахолка")
    private WebElement abLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Люди")
    private WebElement peopleLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Недвижимость")
    private WebElement realtLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Барахолка")
    private WebElement baraholkaLink;
    @FindBy(xpath = "//a[@class='b-main-navigation__link' and text()='Форум")
    private WebElement forumLink;


    public Navigation(RemoteWebDriver driver) {
        super(driver);
    }

    public CatalogPage navigateToCatalog() {
        logger.debug("Catalog page");
        catalogLink.click();
        return new CatalogPage(driver);
    }
}
