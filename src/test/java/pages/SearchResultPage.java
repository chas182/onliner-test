package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import util.BuildXpath;
import util.FilterParam;
import util.Order;
import util.SortBy;
import util.exceptions.SortException;

import java.util.LinkedList;
import java.util.List;

/**
 * @author vabramov
 */
public class SearchResultPage extends ContentPage {

    @FindBy(xpath = "//strong[@class='pname']/a")
    private List<WebElement> productsLinks;
    @FindBy(xpath = "//select[@id='lmfr0']")
    private WebElement producerSelect;
    @FindBy(xpath = "//input[@name='search']")
    private WebElement submitAdvancedSearchButton;

    public SearchResultPage(RemoteWebDriver driver) {
        super(driver);
    }

    public SearchResultPage sort(SortBy sortBy, Order order) {
        WebElement sortLink = getSortLink(sortBy);
        if (!order.equals(Order.getOrder(sortLink))) {
            sortLink.click();
            sortLink = getSortLink(sortBy);
            if (!order.equals(Order.getOrder(sortLink))) {
                sortLink.click();
                sortLink = getSortLink(sortBy);
                if (!order.equals(Order.getOrder(sortLink))) {
                    throw new SortException("Could not find '" + order.getCssClassName() + "' css class in link after two clicks.");
                }
            }
        }
        logger.info("Products are sorted by '" + sortBy.name() + "' by '" + order.name() + "' order");
        return this;
    }

    private WebElement getSortLink(SortBy sortBy) {
        return driver.findElement(By.xpath("//a[text()='" + sortBy.getLinkText() + "']"));
    }

    public List<String> getProductsLinksText() {
        List<String> result = new LinkedList<String>();
        for (WebElement link : productsLinks) {
            result.add(link.getText());
        }
        return result;
    }

    public ProductPage getFirstProduct() {
        if (!productsLinks.isEmpty()) {
            productsLinks.get(0).click();
            return new ProductPage(driver);
        }
        return null;
    }

    public SearchResultPage selectProducer(String producer) {
        new Select(producerSelect).selectByValue(producer.toLowerCase());
        return this;
    }
    //input[contains(@name, 'sp[price]')][2]
    public SearchResultPage setParams(FilterParam[] params) {
        for (FilterParam param : params) {
            List<WebElement> elems;
            if (param.isSelect()) {
                elems = param.getElementInFilterSection(driver);
                if (!elems.isEmpty()) {
                    new Select(elems.get(0)).selectByVisibleText(param.getValue());
                }
            } else {
                elems = param.getElementInFilterSection(driver);
                if (elems.size() >= 2) {
                    elems.get(0).sendKeys(String.valueOf(param.getFrom()));
                    elems.get(1).sendKeys(String.valueOf(param.getTo()));
                }
            }
        }
        submitAdvancedSearchButton.click();
        return this;
    }
}
