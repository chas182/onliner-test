package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * @author vabramov
 */
public abstract class ContentPage extends PageBase {

    @FindBy(xpath = "//input[@id='search-input']")
    private WebElement searchInput;
    @FindBy(xpath = "//input[@class='onlsearch__btn']")
    private WebElement searchButton;

    public ContentPage(RemoteWebDriver driver) {
        super(driver);
    }

    public SearchResultPage search(String whatToType) {
        searchInput.sendKeys(whatToType);
        searchButton.click();
        return new SearchResultPage(driver);
    }

}
