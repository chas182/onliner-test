package pages;

import ch.qos.logback.classic.Level;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @author vabramov
 */
public abstract class PageBase {

    protected final RemoteWebDriver driver;
    protected final Logger logger;
    protected final String baseUrl;
    protected final WebDriverWait wait;

    public PageBase(RemoteWebDriver driver) {
        this.driver = driver;
        this.logger = LoggerFactory.getLogger(this.getClass().getSimpleName());
        this.baseUrl = "http://www.onliner.by/";
        this.wait = new WebDriverWait(this.driver, 10);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(driver, this);
    }

    public Navigation navigation() {
        logger.debug("Navigate to ->");
        return new Navigation(driver);
    }

    public RemoteWebDriver getDriver() {
        return driver;
    }

    public MainPage startPage() {
        return new MainPage(driver);
    }

    public abstract SearchResultPage search(String phrase);

    protected void getPage(String url) {
        logger.info("Get '" + url + "' page");
        long start = System.currentTimeMillis();
        driver.get(url);
        logger.info("Page '" + this.driver.getCurrentUrl() + "' is successfully retrieved ({} ms)", System.currentTimeMillis() - start);
    }
}
